package Database

import (
	"fmt"
	//"gitlab.com/victorrb1015/Delta_mf/App/Model"
	config "gitlab.com/victorrb1015/delta_mf/Config"
	model "gitlab.com/victorrb1015/delta_mf/App/Model"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"log"
	"strconv"
)

var DbC *gorm.DB

func ConnectdbClient(DbName string) {
	var err error
	p := config.Config("DB_PORT")
	port, err := strconv.ParseUint(p, 10, 32)

	if err != nil {
		log.Println("Idiot")
	}

	// Connection URL to connect to Client Database
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local", config.Config("DB_USER"), config.Config("DB_PASSWORD"), config.Config("DB_HOST"), port, DbName)

	DbC, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})

	if err != nil {
		panic("failed to connect database client")
	}

	
	fmt.Println("Connection Opened to Database")
	if err = DbC.AutoMigrate(&model.DeltaType{}, &model.DeltaMeasure{}, &model.DeltaUbication{}, &model.DeltaColor{}, &model.DeltaItem{}, &model.DeltaGauge{}, 
			&model.DeltaAdjustment{}, &model.DeltaRequisition{}, &model.DeltaInventory{}); err != nil {
		panic("failed to migrate database client")
	}
	//if err = DbC.AutoMigrate(&Model.BuildingsOrdersNotes{}, &Model.BuildingsOrderDetails{}, &Model.BuildingsOrdersOptionalDetails{}, &Model.BuildingsOrdersOptional{}, &Model.BuildingsOrders{}); err == nil && DbC.Migrator().HasTable(&Model.BuildingsOrders{}) {
	/*if err := DbC.First(&model.Status{}).Error; errors.Is(err, gorm.ErrRecordNotFound) {
		//Insert seed data
		for i, _ := range seed.Status {
			err = DbC.Debug().Model(&model.Status{}).Create(&seed.Status[i]).Error
			if err != nil {
				log.Fatalf("cannot seed Status table: %v", err)
			}
		}
		for i, _ := range seed.Payments {
			err = DbC.Debug().Model(&model.Payment{}).Create(&seed.Payments[i]).Error
			if err != nil {
				log.Fatalf("cannot seed Payments table: %v", err)
			}
		}
		for i, _ := range seed.PaymentsTerms {
			err = DbC.Debug().Model(&model.PaymentTerms{}).Create(&seed.PaymentsTerms[i]).Error
			if err != nil {
				log.Fatalf("cannot seed Payments table: %v", err)
			}
		}
		for i, _ := range seed.ClientTypes {
			err = DbC.Debug().Model(&model.ClientTypes{}).Create(&seed.ClientTypes[i]).Error
			if err != nil {
				log.Fatalf("cannot seed ClientTypes table: %v", err)
			}
		}
		for i, _ := range seed.Category {
			err = DbC.Debug().Model(&model.Category{}).Create(&seed.Category[i]).Error
			if err != nil {
				log.Fatalf("cannot seed Category table: %v", err)
			}
		}
	}*/
	//}
	if err != nil {
		panic("failed to Migrated database client")
	}
	fmt.Println(" Database " + DbName + " Migrated ")
}
