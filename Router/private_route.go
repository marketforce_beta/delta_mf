package Router

import (
	"github.com/gofiber/fiber/v2"
	colorsRoutes "gitlab.com/victorrb1015/delta_mf/App/Route/Inventory/Colors"
	ubicationsRoutes "gitlab.com/victorrb1015/delta_mf/App/Route/Items/Ubications"
	itemsRoutes "gitlab.com/victorrb1015/delta_mf/App/Route/Items"
	gaugesRoutes "gitlab.com/victorrb1015/delta_mf/App/Route/Inventory/Gauge"
	typesRoutes "gitlab.com/victorrb1015/delta_mf/App/Route/Inventory/Type"
	measureRoutes "gitlab.com/victorrb1015/delta_mf/App/Route/Inventory/Measure"
	requisitionRoutes "gitlab.com/victorrb1015/delta_mf/App/Route/Inventory/Requisition"
	adjustmentRoutes "gitlab.com/victorrb1015/delta_mf/App/Route/Inventory/Adjustment"
	inventoryRoutes "gitlab.com/victorrb1015/delta_mf/App/Route/Inventory"
)

func PrivateRoutes(app *fiber.App) {
	api := app.Group("/api")
	colorsRoutes.SetupColorsRoute(api)
	ubicationsRoutes.SetupUbicationsRoute(api)
	itemsRoutes.SetupItemsRoute(api)
	gaugesRoutes.SetupGaugesRoute(api)
	typesRoutes.SetupTypesRoute(api)
	measureRoutes.SetupMeasureRoute(api)
	requisitionRoutes.SetupRequisitionsRoute(api)
	adjustmentRoutes.SetupAdjustmentRoute(api)
	inventoryRoutes.SetupInventoryRoutes(api)

}