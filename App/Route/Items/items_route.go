package Items

import (
	"github.com/gofiber/fiber/v2"
	itemsController "gitlab.com/victorrb1015/delta_mf/App/Controller/Items"
)

func SetupItemsRoute(router fiber.Router) {
	items := router.Group("/items")
	// Get all items
	items.Get("/:database", itemsController.GetAllItems)
	// Create item
	items.Post("/:database", itemsController.CreateItem)
	// Get item
	items.Get("/:database/:id", itemsController.GetItem)
	// Update item
	items.Put("/:database/:id", itemsController.UpdateItem)
	// Delete item
	items.Delete("/:database/:id", itemsController.DeleteItem)
}