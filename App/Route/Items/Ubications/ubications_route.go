package Ubications

import (
	"github.com/gofiber/fiber/v2"
	ubicationsController "gitlab.com/victorrb1015/delta_mf/App/Controller/Items/Ubications"
)

func SetupUbicationsRoute(router fiber.Router) {
	ubications := router.Group("/ubications")
	//Get all ubications
	ubications.Get("/:database/", ubicationsController.GetUbications)
	//Create a new ubication
	ubications.Post("/:database/", ubicationsController.CreateUbication)
	//Get a ubication by id
	ubications.Get("/:database/:id", ubicationsController.GetUbication)
	//Update a ubication
	ubications.Put("/:database/:id", ubicationsController.UpdateUbication)
	//Delete a ubication
	ubications.Delete("/:database/:id", ubicationsController.DeleteUbication)
	

}
