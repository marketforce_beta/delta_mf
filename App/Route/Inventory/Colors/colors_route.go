package Colors

import (
	"github.com/gofiber/fiber/v2"
	colorsController "gitlab.com/victorrb1015/delta_mf/App/Controller/Inventory/Colors"
)

func SetupColorsRoute(router fiber.Router) {
	colors := router.Group("/colors")
	//Get all colors
	colors.Get("/:database/", colorsController.GetColors)
	//Create a new color
	colors.Post("/:database/", colorsController.CreateColor)
	//Get a color by id
	colors.Get("/:database/:id", colorsController.GetColor)
	//Update a color
	colors.Put("/:database/:id", colorsController.UpdateColor)
	//Delete a color
	colors.Delete("/:database/:id", colorsController.DeleteColor)

}