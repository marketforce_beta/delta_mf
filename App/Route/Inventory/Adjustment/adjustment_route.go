package Adjustments

import (
	"github.com/gofiber/fiber/v2"
	adjustmentController "gitlab.com/victorrb1015/delta_mf/App/Controller/Inventory/Adjustment"
)

func SetupAdjustmentRoute(router fiber.Router) {
	adjustment := router.Group("/adjustment")
	//All Adjustments
	adjustment.Get("/:database/", adjustmentController.GetAllAdjustments)
	//Create Adjustment
	adjustment.Post("/:database/", adjustmentController.CreateAdjustment)
	//Get Adjustment
	adjustment.Get("/:database/:id", adjustmentController.GetAdjustment)
	//Update Adjustment
	adjustment.Put("/:database/:id", adjustmentController.UpdateAdjustment)
	//Delete Adjustment
	adjustment.Delete("/:database/:id", adjustmentController.DeleteAdjustment)
}