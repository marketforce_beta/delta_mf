package Gauges

import (
	"github.com/gofiber/fiber/v2"
	gaugesController "gitlab.com/victorrb1015/delta_mf/App/Controller/Inventory/Gauge"
)

func SetupGaugesRoute(router fiber.Router){
	gauges := router.Group("/gauges")
	//All Gauges
	gauges.Get("/:database/", gaugesController.GetAllGauges)
	//Create Gauge
	gauges.Post("/:database/", gaugesController.CreateGauge)
	//Get Gauge
	gauges.Get("/:database/:id", gaugesController.GetGauge)
	//Update Gauge
	gauges.Put("/:database/:id", gaugesController.UpdateGauge)
	//Delete Gauge
	gauges.Delete("/:database/:id", gaugesController.DeleteGauge)
}