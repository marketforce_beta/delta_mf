package Requisitions

import (
	"github.com/gofiber/fiber/v2"
	requisitionController "gitlab.com/victorrb1015/delta_mf/App/Controller/Inventory/Requisition"
)

func SetupRequisitionsRoute(router fiber.Router) {
	requisition := router.Group("/requisition")
	//All Requisitions
	requisition.Get("/:database/", requisitionController.GetAllRequisitions)
	//Create Requisition
	requisition.Post("/:database/", requisitionController.CreateRequisition)
	//Get Requisition
	requisition.Get("/:database/:id", requisitionController.GetRequisition)
	//Update Requisition
	requisition.Put("/:database/:id", requisitionController.UpdateRequisition)
	//Delete Requisition
	requisition.Delete("/:database/:id", requisitionController.DeleteRequisition)
}