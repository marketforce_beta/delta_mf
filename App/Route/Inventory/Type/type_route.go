package DeltaTypes

import (
	"github.com/gofiber/fiber/v2"
	typeController "gitlab.com/victorrb1015/delta_mf/App/Controller/Inventory/Type"
)

func SetupTypesRoute(router fiber.Router) {
	types := router.Group("/types")
	//All Types
	types.Get("/:database/", typeController.GetAllTypes)
	//Create Type
	types.Post("/:database/", typeController.CreateType)
	//Get Type
	types.Get("/:database/:id", typeController.GetType)
	//Update Type
	types.Put("/:database/:id", typeController.UpdateType)
	//Delete Type
	types.Delete("/:database/:id", typeController.DeleteType)
}