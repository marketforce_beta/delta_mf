package Inventories

import (
	"github.com/gofiber/fiber/v2"
	inventoryController "gitlab.com/victorrb1015/delta_mf/App/Controller/Inventory"
)

func SetupInventoryRoutes(router fiber.Router) {
	inventory := router.Group("/inventory")
	//All Inventory
	inventory.Get("/:database/", inventoryController.GetAllInventory)
	//Create Inventory
	inventory.Post("/:database/", inventoryController.CreateInventory)
	//Get Inventory
	inventory.Get("/:database/:id", inventoryController.GetInventory)
	//Update Inventory
	inventory.Put("/:database/:id", inventoryController.UpdateInventory)
	//Delete Inventory
	inventory.Delete("/:database/:id", inventoryController.DeleteInventory)
}