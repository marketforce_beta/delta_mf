package Measures

import (
	"github.com/gofiber/fiber/v2"
	measureController "gitlab.com/victorrb1015/delta_mf/App/Controller/Inventory/Measure"
)

func SetupMeasureRoute(router fiber.Router) {
	measure := router.Group("/measure")
	//All Measures
	measure.Get("/:database/", measureController.GetAllMeasures)
	//Create Measure
	measure.Post("/:database/", measureController.CreateMeasure)
	//Get Measure
	measure.Get("/:database/:id", measureController.GetMeasure)
	//Update Measure
	measure.Put("/:database/:id", measureController.UpdateMeasure)
	//Delete Measure
	measure.Delete("/:database/:id", measureController.DeleteMeasure)
}