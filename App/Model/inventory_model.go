package Model

import (
	"gorm.io/gorm"
	"time"
)

type DeltaType struct {
	gorm.Model
	Type string `gorm:"type:varchar(255);not null" db:"type" valid:"required,lte=255" json:"type"`
	Description string `gorm:"type:varchar(255);not null" db:"description" valid:"required,lte=255" json:"description"`
	DeltaInventory []DeltaInventory `gorm:"foreignkey:DeltaTypeID"`
}

type DeltaMeasure struct {
	gorm.Model
	Name string `gorm:"type:varchar(255);not null" db:"name" valid:"required,lte=255" json:"name"`
	DeltaInventory []DeltaInventory `gorm:"foreignkey:DeltaMeasureID"`	
}

type DeltaUbication struct {
	gorm.Model
	Name string `gorm:"type:varchar(255);not null" db:"name" valid:"required,lte=255" json:"name"`
	DeltaItem []DeltaItem `gorm:"foreignkey:DeltaUbicationID"`
}

type DeltaColor struct {
	gorm.Model
	Name string `gorm:"type:varchar(255);not null" db:"name" valid:"required,lte=255" json:"name"`
	DeltaInventory []DeltaInventory `gorm:"foreignkey:DeltaColorID"`
}

type DeltaItem struct {
	gorm.Model
	CodeBar string `gorm:"type:varchar(255);not null" db:"code_bar" valid:"required,lte=255" json:"code_bar"`
	Length float64 `gorm:"type:float(10,2);null" db:"length" valid:"required,lte=10.2" json:"length"`
	Weight float64 `gorm:"type:float(10,2);null" db:"weight" valid:"required,lte=10.2" json:"weight"`
	DeltaUbicationID uint `gorm:"type:int;not null" db:"delta_ubication_id" valid:"required,lte=10" json:"delta_ubication_id"`
	DeltaInventoryID uint `gorm:"type:int;not null" db:"delta_inventory_id" valid:"required,lte=10" json:"delta_inventory_id"`
}

type DeltaGauge struct {
	gorm.Model
	Name string `gorm:"type:varchar(255);not null" db:"name" valid:"required,lte=255" json:"name"`
	DeltaInventory []DeltaInventory `gorm:"foreignkey:DeltaGaugeID"`
}

type DeltaAdjustment struct {
	gorm.Model
	LastQuantity float64 `gorm:"type:float(10,2);not null" db:"last_quantity" valid:"required,lte=10.2" json:"last_quantity"`
	Quantity float64 `gorm:"type:float(10,2);not null" db:"quantity" valid:"required,lte=10.2" json:"quantity"`
	NewQuantity float64 `gorm:"type:float(10,2);not null" db:"new_quantity" valid:"required,lte=10.2" json:"new_quantity"`
	Reason string `gorm:"type:varchar(255);not null" db:"reason" valid:"required,lte=255" json:"reason"`
	DeltaInventoryID uint `gorm:"type:int;not null" db:"delta_inventory_id" valid:"required,lte=10" json:"delta_inventory_id"`
}

type DeltaRequisition struct {
	gorm.Model
	DateArrived time.Time `gorm:"type:datetime;null;default:CURRENT_TIMESTAMP" db:"date_arrived" valid:"lte=10.2" json:"date_arrived"`
	QuantityArrived float64 `gorm:"type:float(10,2);null" db:"quantity_arrived" valid:"lte=10.2" json:"quantity_arrived"`
	NumberOrder string `gorm:"type:varchar(255);not null" db:"number_order" valid:"required,lte=255" json:"number_order"`
	EstimatedDate time.Time `gorm:"type:datetime;not null" db:"estimated_date" valid:"required,lte=10.2" json:"estimated_date"`
	QuantityEstimated float64 `gorm:"type:float(10,2);not null" db:"quantity_estimated" valid:"required,lte=10.2" json:"quantity_estimated"`
	DeltaInventoryID uint `gorm:"type:int;not null" db:"delta_inventory_id" valid:"required,lte=10" json:"delta_inventory_id"`
}

type DeltaInventory struct {
	gorm.Model
	Name   string	`gorm:"type:varchar(100);not null" db:"name" valid:"required,lte=255" json:"name"`
	Description string `gorm:"type:varchar(255);not null" db:"description" valid:"required,lte=255" json:"description"`
	Cost float64 `gorm:"type:float(10,2);null" db:"cost" valid:"required,lte=10.2" json:"cost"`
	Price float64 `gorm:"type:float(10,2);null" db:"price" valid:"required,lte=10.2" json:"price"`
	Weight float64 `gorm:"type:float(10,2);null" db:"weight" valid:"required,lte=10.2" json:"weight"`
	Quantity float64 `gorm:"type:float(10,2);default:0;" db:"quantity" valid:"required,lte=10.2" json:"quantity"`
	MinQuantity float64 `gorm:"type:float(10,2);default:0;" db:"min_quantity" valid:"required,lte=10.2" json:"min_quantity"`
	Length float64 `gorm:"type:float(10,2);null" db:"length" valid:"required,lte=10.2" json:"length"`
	DeltaTypeID uint `gorm:"type:int;not null" db:"delta_type_id" valid:"required,lte=10" json:"delta_type_id"`
	DeltaMeasureID uint `gorm:"type:int;not null" db:"delta_measure_id" valid:"required,lte=10" json:"delta_measure_id"`
	DeltaGaugeID uint `gorm:"type:int;not null" db:"delta_gauge_id" valid:"required,lte=10" json:"delta_gauge_id"`
	DeltaColorID uint `gorm:"type:int;not null" db:"delta_color_id" valid:"required,lte=10" json:"delta_color_id"`
	DeltaItem []DeltaItem `gorm:"foreignkey:DeltaInventoryID"`
	DeltaAdjustment []DeltaAdjustment `gorm:"foreignkey:DeltaInventoryID"`
	DeltaRequisition []DeltaRequisition `gorm:"foreignkey:DeltaInventoryID"`
}