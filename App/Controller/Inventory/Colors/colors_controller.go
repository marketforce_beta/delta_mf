package Colors

import (
	"github.com/gofiber/fiber/v2"
	model "gitlab.com/victorrb1015/delta_mf/App/Model"
	utils "gitlab.com/victorrb1015/delta_mf/App/Utils"
	database "gitlab.com/victorrb1015/delta_mf/Database"
)

func GetColors(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	var colors []model.DeltaColor 
	db.Find(&colors)
	if len(colors) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No colors present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Colors Found", "data": colors})
}


func CreateColor(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	var color model.DeltaColor
	if err := c.BodyParser(&color); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": nil})
	}
	db.Create(&color)
	return c.JSON(fiber.Map{"status": "success", "message": "Color Created", "data": color})
}

func GetColor(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	id := c.Params("id")
	var color model.DeltaColor
	err := db.First(&color, id).Error
	if err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Color not found", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Color Found", "data": color})
}

func UpdateColor(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	id := c.Params("id")
	var color model.DeltaColor
	var colorUpdate model.DeltaColor
	if err := c.BodyParser(&color); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": nil})
	}
	err := db.First(&colorUpdate, id).Error
	if err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Color not found", "data": err})
	}
	db.Model(&color).Where("id = ?", id).Updates(color)
	return c.JSON(fiber.Map{"status": "success", "message": "Color Updated", "data": color})
}

func DeleteColor(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	id := c.Params("id")
	var color model.DeltaColor
	err := db.First(&color, id).Error
	if err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Color not found", "data": err})
	}
	db.Delete(&color)
	return c.JSON(fiber.Map{"status": "success", "message": "Color Deleted", "data": nil})
}