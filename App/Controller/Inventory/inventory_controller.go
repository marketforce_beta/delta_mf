package Inventory

import (
	"github.com/gofiber/fiber/v2"
	model "gitlab.com/victorrb1015/delta_mf/App/Model"
	utils "gitlab.com/victorrb1015/delta_mf/App/Utils"
	database "gitlab.com/victorrb1015/delta_mf/Database"
)

func GetAllInventory(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	var inventories []model.DeltaInventory
	db.Find(&inventories)
	if len(inventories) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No Inventory present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Inventory list", "data": inventories})
}

func CreateInventory(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	var deltaInventory model.DeltaInventory
	if err := c.BodyParser(&deltaInventory); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": err})
	}
	db.Create(&deltaInventory)
	return c.JSON(fiber.Map{"status": "success", "message": "Inventory Created", "data": deltaInventory})
}

func GetInventory(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	var deltaInventory model.DeltaInventory
	db.First(&deltaInventory, c.Params("id"))
	if deltaInventory.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Inventory not found", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Inventory found", "data": deltaInventory})
}

func UpdateInventory(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	id := c.Params("id")
	var deltaInventory model.DeltaInventory
	var deltaInventoryUpdate model.DeltaInventory
	if err := c.BodyParser(&deltaInventory); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": err})
	}
	err := db.First(&deltaInventoryUpdate, id).Error
	if err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Inventory not found", "data": err})
	}
	db.Model(&deltaInventory).Where("id = ?", id).Updates(deltaInventory)
	return c.JSON(fiber.Map{"status": "success", "message": "Inventory updated", "data": deltaInventory})
	
}

func DeleteInventory(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	var deltaInventory model.DeltaInventory
	err := db.First(&deltaInventory, c.Params("id")).Error
	if err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Inventory not found", "data": err})
	}
	db.Delete(&deltaInventory)
	return c.JSON(fiber.Map{"status": "success", "message": "Inventory deleted", "data": nil})
}