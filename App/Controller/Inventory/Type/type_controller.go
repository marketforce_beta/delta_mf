package DeltaType

import (
	"github.com/gofiber/fiber/v2"
	model "gitlab.com/victorrb1015/delta_mf/App/Model"
	utils "gitlab.com/victorrb1015/delta_mf/App/Utils"
	database "gitlab.com/victorrb1015/delta_mf/Database"
)

func GetAllTypes(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	var types []model.DeltaType
	db.Find(&types)
	if len(types) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No Type present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Type list", "data": types})
}

func CreateType(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	var deltaType model.DeltaType
	if err := c.BodyParser(&deltaType); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": err})
	}
	db.Create(&deltaType)
	return c.JSON(fiber.Map{"status": "success", "message": "Type Created", "data": deltaType})
}

func GetType(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	var deltaType model.DeltaType
	db.First(&deltaType, c.Params("id"))
	if deltaType.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Type not found", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Type found", "data": deltaType})
}

func UpdateType(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	id := c.Params("id")
	var deltaType model.DeltaType
	var deltaTypeNew model.DeltaType
	if err := c.BodyParser(&deltaType); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": err})
	}
	err := db.First(&deltaTypeNew, id).Error
	if err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Type not found", "data": err})
	}
	db.Model(&deltaTypeNew).Where("id = ?", id).Updates(deltaType)
	return c.JSON(fiber.Map{"status": "success", "message": "Type Updated", "data": deltaType})
}

func DeleteType(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	id := c.Params("id")
	var deltaType model.DeltaType
	err := db.First(&deltaType, id).Error
	if err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Type not found", "data": err})
	}
	db.Delete(&deltaType)
	return c.JSON(fiber.Map{"status": "success", "message": "Type Deleted", "data": nil})
}