package Measure

import (
	"github.com/gofiber/fiber/v2"
	model "gitlab.com/victorrb1015/delta_mf/App/Model"
	utils "gitlab.com/victorrb1015/delta_mf/App/Utils"
	database "gitlab.com/victorrb1015/delta_mf/Database"
)

func GetAllMeasures(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	var measures []model.DeltaMeasure
	db.Find(&measures)
	if len(measures) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No Measure present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Measure list", "data": measures})
}

func CreateMeasure(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	var deltaMeasure model.DeltaMeasure
	if err := c.BodyParser(&deltaMeasure); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": err})
	}
	db.Create(&deltaMeasure)
	return c.JSON(fiber.Map{"status": "success", "message": "Measure Created", "data": deltaMeasure})
}

func GetMeasure(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	var deltaMeasure model.DeltaMeasure
	db.First(&deltaMeasure, c.Params("id"))
	if deltaMeasure.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Measure not found", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Measure found", "data": deltaMeasure})
}

func UpdateMeasure(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	id := c.Params("id")
	var deltaMeasure model.DeltaMeasure
	var deltaMeasureNew model.DeltaMeasure
	if err := c.BodyParser(&deltaMeasure); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": err})
	}
	err := db.First(&deltaMeasureNew, id).Error
	if err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Measure not found", "data": err})
	}
	db.Model(&deltaMeasure).Where("id = ?", id).Updates(deltaMeasure)
	return c.JSON(fiber.Map{"status": "success", "message": "Measure Updated", "data": deltaMeasure})
}

func DeleteMeasure(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	id := c.Params("id")
	var deltaMeasure model.DeltaMeasure
	err := db.First(&deltaMeasure, id).Error
	if err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Measure not found", "data": err})
	}
	db.Delete(&deltaMeasure)
	return c.JSON(fiber.Map{"status": "success", "message": "Measure Deleted", "data": nil})
}