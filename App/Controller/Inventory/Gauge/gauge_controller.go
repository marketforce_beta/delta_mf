package Gauge

import (
	"github.com/gofiber/fiber/v2"
	model "gitlab.com/victorrb1015/delta_mf/App/Model"
	utils "gitlab.com/victorrb1015/delta_mf/App/Utils"
	database "gitlab.com/victorrb1015/delta_mf/Database"
)

func GetAllGauges(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	var gauges []model.DeltaGauge
	db.Find(&gauges)
	if len(gauges) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No Gauge present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Gauge list", "data": gauges})

}

func CreateGauge(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	var gauge model.DeltaGauge
	if err := c.BodyParser(&gauge); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": err})
	}
	db.Create(&gauge)
	return c.JSON(fiber.Map{"status": "success", "message": "Gauge Created", "data": gauge})
}

func GetGauge(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	var gauge model.DeltaGauge
	db.First(&gauge, c.Params("id"))
	if gauge.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Gauge not found", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Gauge found", "data": gauge})
}

func UpdateGauge(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	id := c.Params("id")
	var gauge model.DeltaGauge
	var newGauge model.DeltaGauge
	if err := c.BodyParser(&gauge); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": err})
	}
	err := db.First(&newGauge, id).Error
	if err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "gauge not found", "data": err})
	}
	db.Model(&gauge).Where("id = ?", id).Updates(gauge)
	return c.JSON(fiber.Map{"status": "success", "message": "Gauge Updated", "data": gauge})
}

func DeleteGauge(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	id := c.Params("id")
	var gauge model.DeltaGauge
	
	err := db.First(&gauge, id).Error
	if err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Gauge not found", "data": err})
	}
	db.Delete(&gauge)
	return c.JSON(fiber.Map{"status": "success", "message": "Gauge Deleted", "data": nil})
}