package Requisition

import (
	"github.com/gofiber/fiber/v2"
	model "gitlab.com/victorrb1015/delta_mf/App/Model"
	utils "gitlab.com/victorrb1015/delta_mf/App/Utils"
	database "gitlab.com/victorrb1015/delta_mf/Database"
)

func GetAllRequisitions(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	var requisitions []model.DeltaRequisition
	db.Find(&requisitions)
	if len(requisitions) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No Requisition present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Requisition list", "data": requisitions})
}

func CreateRequisition(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	var deltaRequisition model.DeltaRequisition

	if err := c.BodyParser(&deltaRequisition); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": err})
	}
	db.Create(&deltaRequisition)
	return c.JSON(fiber.Map{"status": "success", "message": "Requisition Created", "data": deltaRequisition})
}

func GetRequisition(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	var deltaRequisition model.DeltaRequisition
	db.First(&deltaRequisition, c.Params("id"))
	if deltaRequisition.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Requisition not found", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Requisition found", "data": deltaRequisition})
}

func UpdateRequisition(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	id := c.Params("id")
	var deltaRequisition model.DeltaRequisition
	if err := c.BodyParser(&deltaRequisition); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": err})
	}
	err := db.First(&deltaRequisition, id).Error
	if err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Requisition not found", "data": err})
	}
	db.Save(&deltaRequisition)
	return c.JSON(fiber.Map{"status": "success", "message": "Requisition Updated", "data": deltaRequisition})
}

func DeleteRequisition(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	id := c.Params("id")
	var deltaRequisition model.DeltaRequisition
	err := db.First(&deltaRequisition, id).Error
	if err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Requisition not found", "data": err})
	}
	db.Delete(&deltaRequisition)
	return c.JSON(fiber.Map{"status": "success", "message": "Requisition Deleted", "data": nil})
}
