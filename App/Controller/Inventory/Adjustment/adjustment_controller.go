package Adjustment

import (
	"github.com/gofiber/fiber/v2"
	model "gitlab.com/victorrb1015/delta_mf/App/Model"
	utils "gitlab.com/victorrb1015/delta_mf/App/Utils"
	database "gitlab.com/victorrb1015/delta_mf/Database"
)

func GetAllAdjustments(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	var adjustments []model.DeltaAdjustment
	db.Find(&adjustments)
	if len(adjustments) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No Adjustment present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Adjustment list", "data": adjustments})
}

func CreateAdjustment(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	var deltaAdjustment model.DeltaAdjustment
	if err := c.BodyParser(&deltaAdjustment); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": err})
	}
	db.Create(&deltaAdjustment)
	return c.JSON(fiber.Map{"status": "success", "message": "Adjustment Created", "data": deltaAdjustment})
}

func GetAdjustment(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	var deltaAdjustment model.DeltaAdjustment
	db.First(&deltaAdjustment, c.Params("id"))
	if deltaAdjustment.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Adjustment not found", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Adjustment found", "data": deltaAdjustment})
}

func UpdateAdjustment(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	id := c.Params("id")
	var deltaAdjustment model.DeltaAdjustment
	if err := c.BodyParser(&deltaAdjustment); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": err})
	}
	err := db.First(&deltaAdjustment, id).Error
	if err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Requisition not found", "data": err})
	}
	db.Save(&deltaAdjustment)
	return c.JSON(fiber.Map{"status": "success", "message": "Adjustment Updated", "data": deltaAdjustment})
}

func DeleteAdjustment(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	id := c.Params("id")
	var deltaAdjustment model.DeltaAdjustment
	err := db.First(&deltaAdjustment, id).Error
	if err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Requisition not found", "data": err})
	}
	db.Delete(&deltaAdjustment)
	return c.JSON(fiber.Map{"status": "success", "message": "Adjustment Deleted", "data": nil})
}