package Item

import (
	"github.com/gofiber/fiber/v2"
	model "gitlab.com/victorrb1015/delta_mf/App/Model"
	utils "gitlab.com/victorrb1015/delta_mf/App/Utils"
	database "gitlab.com/victorrb1015/delta_mf/Database"
)

func GetAllItems(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	var items []model.DeltaItem
	db.Find(&items)
	if len(items) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No items present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Items Found", "data": items})
}

func CreateItem(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	var item model.DeltaItem
	if err := c.BodyParser(&item); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": nil})
	}
	if err := db.Create(&item).Error; err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": err})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Item Created", "data": item})
}

func GetItem(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	id := c.Params("id")
	var item model.DeltaItem
	if err := db.First(&item, id).Error; err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Item not found", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Item Found", "data": item})
}

func UpdateItem(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	var item model.DeltaItem
	if err := db.First(&item, c.Params("id")).Error; err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Item not found", "data": nil})
	}
	if err := c.BodyParser(&item); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": err})
	}
	db.Save(&item)
	return c.JSON(fiber.Map{"status": "success", "message": "Item Updated", "data": item})
}

func DeleteItem(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	var item model.DeltaItem
	if err := db.First(&item, c.Params("id")).Error; err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Item not found", "data": nil})
	}
	db.Delete(&item)
	return c.JSON(fiber.Map{"status": "success", "message": "Item Deleted", "data": nil})
}