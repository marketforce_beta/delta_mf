package Ubications

import (
	"github.com/gofiber/fiber/v2"
	model "gitlab.com/victorrb1015/delta_mf/App/Model"
	utils "gitlab.com/victorrb1015/delta_mf/App/Utils"
	database "gitlab.com/victorrb1015/delta_mf/Database"
)

func GetUbications(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	var ubications []model.DeltaUbication
	db.Find(&ubications)
	if len(ubications) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No ubications present", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Ubications Found", "data": ubications})
}

func CreateUbication(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	var ubication model.DeltaUbication
	if err := c.BodyParser(&ubication); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": nil})
	}
	if err := db.Create(&ubication).Error; err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": err})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Ubication Created", "data": ubication})
}

func GetUbication(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	var ubication model.DeltaUbication
	if err := db.First(&ubication, c.Params("id")).Error; err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Ubication not found", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Ubication Found", "data": ubication})
}

func UpdateUbication(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	id := c.Params("id")
	var ubication model.DeltaUbication
	var ubicationUpdate model.DeltaUbication
	if err := c.BodyParser(&ubication); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": err})
	}
	if err := db.First(&ubicationUpdate, id).Error; err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Ubication not found", "data": nil})
	}

	db.Model(&ubication).Where("id = ?", id).Updates(ubication)
	return c.JSON(fiber.Map{"status": "success", "message": "Ubication Updated", "data": ubication})
}

func DeleteUbication(c *fiber.Ctx) error {
	data := c.Params("database")
	utils.ConnectClient(data)
	db := database.DbC
	var ubication model.DeltaUbication
	if err := db.First(&ubication, c.Params("id")).Error; err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Ubication not found", "data": nil})
	}
	if err := db.Delete(&ubication).Error; err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Invalid body", "data": err})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Ubication Deleted", "data": ubication})
}
