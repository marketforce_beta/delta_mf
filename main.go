package main

import (
	"github.com/gofiber/fiber/v2"
	middleware "gitlab.com/victorrb1015/delta_mf/App/Middleware"
	utils "gitlab.com/victorrb1015/delta_mf/App/Utils"
	router "gitlab.com/victorrb1015/delta_mf/Router"

)

func main()  {
	// Initialize the app
	app := fiber.New()
	// Middlewares.
	middleware.FiberMiddleware(app) // Register Fiber's middleware for app.

	// Send a string back for GET calls to the endpoint "/"
	app.Get("/", func(c *fiber.Ctx) error {
		err := c.SendString("And the API is UP!")
		return err
	})

	app.Get("/api/:database/data", func(c *fiber.Ctx) error {
		database := c.Params("database")
		utils.ConnectClient(database)
		err := c.SendString("database is up!")
		return err
	})

	//router.PublicRoutes(app)
	router.PrivateRoutes(app)
	//router.NotFoundRoute(app)

	// Start server (with graceful shutdown).
	utils.StartServer(app)
}